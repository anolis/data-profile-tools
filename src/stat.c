/* This file contains code to create/show/destroy a window on screen. */

#include <inttypes.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/mman.h>
#include <signal.h>
#include <curses.h>
#include "include/types.h"
#include "include/util.h"
#include "include/disp.h"
#include "include/reg.h"
#include "include/proc.h"
#include "include/page.h"
#include "include/perf.h"
#include "include/plat.h"
#include "include/damon.h"
#include "include/stat.h"
#include "include/os/os_util.h"
#include "include/os/os_win.h"
#include <sys/syscall.h>        /* Definition of SYS_* constants */
#include <sys/uio.h>            /* Definition of struct iovec type */
#include <sys/capability.h>
#include <sys/prctl.h>

#define SIZE_2M 0x200000
#define MAX_HEATMAP_SIZE 128 /* Cover 256M area */
#define MAX_HOT_SIZE 32

/* If hugearea = 2M */
#define HEATMAP_INDEX(addr) ((addr) >> 21)
int heatmap_stat = 0;
int thp_collapse = 0;
uint64_t start_addr;
uint64_t access_heatmap[MAX_HEATMAP_SIZE];
extern struct damon_proc_t target_procs;

#ifndef __NR_pidfd_open
#define __NR_pidfd_open 434 /* System call # on most architectures */
#endif

#define MADV_HUGEPAGE 14
#define MADV_NOHUGEPAGE 15
#define MADV_COLLAPSE 25

static int pidfd_open(pid_t pid, unsigned int flags)
{
	return syscall(__NR_pidfd_open, pid, flags);
}

#ifndef __NR_process_madvise
#define __NR_process_madvise 440 /* System call # on most architectures */
#endif

static ssize_t
process_madvise(int pidfd, const struct iovec *iovec, size_t vlen, int advice,
		unsigned int flags)
{
	return syscall(__NR_process_madvise, pidfd, iovec, vlen, advice, flags);
}

void init_stat(void)
{
	memset(access_heatmap, 0, sizeof(uint64_t) * MAX_HEATMAP_SIZE);
}

void add_access_stat(uint64_t addr, uint64_t nr)
{
	if (HEATMAP_INDEX(addr - start_addr) >= MAX_HEATMAP_SIZE) {
		debug_print(NULL, 2, "heatmap overflow: 0x%lx\n", addr);
		return;
	}

	access_heatmap[HEATMAP_INDEX(addr - start_addr)] += nr;
}

void access_data_record(track_proc_t *proc)
{
	count_value_t *cv, *sort_arr;
	int i;
	uint64_t start, nr_accesses;
	int nr_nonzero = proc->nr_nonzero;
	int nr_hotsize = MIN(MAX_HEATMAP_SIZE, nr_nonzero);

	if (!heatmap_stat)
		return;

	sort_arr = zalloc(sizeof(count_value_t) * nr_nonzero);
	if (sort_arr == NULL) {
		return;
	}

	proc_countval_resort(SORT_KEY_NRA, proc);
	memcpy(sort_arr, proc->countval_arr, sizeof(count_value_t) * nr_nonzero);

	/* Now, access data is sorted from large to small. */
	debug_print(NULL, 3, "start_addr: 0x%lx\n", start_addr);
	for (i = 0; i < nr_hotsize; i++) {
		cv = &sort_arr[i];
		start = cv->counts[UI_COUNT_DAMON_START];
		nr_accesses = cv->counts[UI_COUNT_DAMON_NR_ACCESS];
		if (!nr_accesses)
			break;
		add_access_stat(start, nr_accesses);
	}
	free(sort_arr);

	debug_print(NULL, 3, "\n\n");
	for (i = 0; i < MAX_HOT_SIZE; i++) {
		debug_print(NULL, 3, "%d: %d\n", i, access_heatmap[i]);
	}
}

/*
 * The callback function used in qsort() to compare the number
 * of address access.
 */
int addr_hot_cmp(const void *p1, const void *p2)
{
	typedef struct {
		int index;
		uint64_t value;
	} addrhot_kv_t;
	const addrhot_kv_t *l1 = (const addrhot_kv_t *)p1;
	const addrhot_kv_t *l2 = (const addrhot_kv_t *)p2;

	if (l1->value < l2->value) {
		return 1;
	}

	if (l1->value > l2->value) {
		return -1;
	}

	return 0;
}

void heatmap_collapse(void)
{
	pid_t pid;
	int pidfd;
	int ret;
	int i;
	typedef struct {
		int index;
		uint64_t value;
	} addrhot_kv_t;
	addrhot_kv_t collapse[MAX_HEATMAP_SIZE];

	if (!thp_collapse)
		return;

	for (i = 0; i < MAX_HEATMAP_SIZE; i++) {
		collapse[i].index = i;
		collapse[i].value = access_heatmap[i];
	}
	qsort(collapse, MAX_HEATMAP_SIZE, sizeof(addrhot_kv_t), addr_hot_cmp);
	for (i = 0; i < MAX_HOT_SIZE; i++) {
		if (collapse[i].value == 0)
			break;
		debug_print(NULL, 3, "collapse: %d: %d\n",
				collapse[i].index, collapse[i].value);
	}

	pid = target_procs.pid[0];
	pidfd = pidfd_open(pid, 0);
	if (pidfd == -1) {
		perror("pidfd_open");
		return;
	}

	for (i = 0; i < MAX_HOT_SIZE; i++) {
		struct iovec iov = {
			.iov_base = (void *)(start_addr + collapse[i].index * SIZE_2M),
			.iov_len = SIZE_2M
		};

		if (collapse[i].value == 0)
			break;

		ret = process_madvise(pidfd, &iov, 1, MADV_HUGEPAGE, 0);
		if (ret >= 0)
			ret = process_madvise(pidfd, &iov, 1, MADV_COLLAPSE, 0);

		debug_print(NULL, 3, "collapse status: %d\n", ret);
	}

	close(pidfd);
	thp_collapse = 0;
}
